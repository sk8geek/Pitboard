package uk.co.channele.pitboard;

import android.app.DialogFragment;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.widget.TextView;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class PitBoard extends AppCompatActivity implements SensorEventListener, Settings.SettingsListener {

    private final String TAG = "PB";
    private Resources res;
    /* Animation syles */
    public static final int ANIMATE_NONE = 0;
    public static final int ANIMATE_FADE = 1;
    public static final int ANIMATE_RISE = 2;
    public static final int ANIMATE_SINK = 3;
    public static final int ANIMATE_ROLL = 4;
    public static final int ANIMATE_HINGE = 5;
    /* Display modes */
    public static final int DISPLAY_TIMED = 0;
    public static final int DISPLAY_SWIPE = 1;
    /* Activity identifiers */
    private static final int ACTIVITY_ADVANCED = 0;
	/* Default values */
	/* 	I've stored default values here because preferences.xml is limited to
		String values. I want to store int and long values. I could achieve
		this using strings, but why would I? Not going to convert them for no
		reason. A knock on effect though is that I need to override some of
		the Preferences classes. */
    /** The message to display. The default value is stored in strings.xml to allow for i18n. */
    private String message;
    /** Display time of each word in milliseconds. Note that this time effectively
     includes the aniOutDuration below. That value is subtracted from this one
     when calculating when to callback to start the animation. */
    private long wordDuration = 4000;
    /** Blank time between words in milliseconds  */
    private long spaceDuration = 600;
    /** Duration of the animation from blank to display of a word, in milliseconds. */
    private long aniInDuration = 300;
    /** Duration of the animation from word to blank, in milliseconds. */
    private long aniOutDuration = 600;
    private int displayMode = DISPLAY_TIMED;
    private int aniInStyle = ANIMATE_HINGE;
    private int aniOutStyle = ANIMATE_SINK;
    // controls
    private TextView messageView;
    // other
    private Handler timer;
    private Frankenstein victor = new Frankenstein();
    private int elementPointer = 0;
    private String[] messageElements;
    private SharedPreferences appPrefs;
    private int textHeight = 0;
    private DisplayMetrics metrics = new DisplayMetrics();
    private int displayHeight = 540;
    private int displayWidth = 0;
    private ViewPropertyAnimator animator;
    private GestureDetector gestureDetector;
    // sensor related
    private SensorManager sensorMan;
    private Sensor sensor;
    private float[] prevAccel = new float[3];
    private int[] accelChange = new int[3];
    private float damper = 0.5f;
    private long nudgeTime = 0;
    private int xGravityLimit = 5;
    private long nudgeDebounce = 900;
    private int rotationTrigger = 20;
    private int nudgeTrigger = 6;
    private boolean sensorsInitialised = false;
    //
    private Settings settings;
    //private Advanced advanced;
    //private SharedPreferences prefs;
    private Spider spider;
    private View mainView;

    private static final boolean AUTO_HIDE = true;
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;
    private static final int UI_ANIMATION_DELAY = 300;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        res = getResources();
        setContentView(R.layout.pitboard);
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        mainView = findViewById(R.id.fullScreen);
        messageView = findViewById(R.id.message);
        sensorMan = (SensorManager)getSystemService(SENSOR_SERVICE);
        try {
            sensor = sensorMan.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        } catch (NullPointerException e) {
            // FIXME: handle NPE
        }
        spider = new Spider();
        gestureDetector = new GestureDetector(this, spider);
        animator = messageView.animate();
        timer = new Handler();
        displayHeight = metrics.heightPixels;
        displayWidth = metrics.widthPixels;
        setFullScreen();
        messageView.setTextSize(displayHeight / 3.8F);
        messageView.setMinimumWidth(displayWidth);
        messageView.setOnTouchListener(spider);
        messageView.setPivotX(0);
        messageView.setPivotY(displayHeight);
        res = getResources();
        appPrefs = getSharedPreferences("PitBoardPrefs", MODE_PRIVATE);
        // TODO: check to see if the preferences file exists. If not then write it.
        initPrefs();
    }


    void setFullScreen() {
        mainView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }


    /**
     * Cancels any outstanding animations on the message and any callbacks
     * to the animation timer.
     */
    private void cancelTransitions() {
        animator.cancel();
        timer.removeCallbacks(victor);
        sensorMan.unregisterListener(this);
    }

    /** Initialise preferences on first run */
    private void initPrefs() {
        SharedPreferences.Editor prefsEditor = appPrefs.edit();
        prefsEditor.putString("message", message);
        prefsEditor.putLong("wordDuration", wordDuration);
        prefsEditor.putLong("spaceDuration", spaceDuration);
        prefsEditor.putLong("aniInDuration", aniInDuration);
        prefsEditor.putLong("aniOutDuration", aniOutDuration);
        prefsEditor.putInt("displayMode", displayMode);
        prefsEditor.putInt("aniInStyle", aniInStyle);
        prefsEditor.putInt("aniOutStyle", aniOutStyle);
        prefsEditor.apply();
    }

    /**
     * Handle the Set button from the team settings dialog.
     * @param fragment the team settings fragment
     */
    public void onDialogPositiveClick(DialogFragment fragment) {
        Bundle state;
        if (fragment.getTag().equalsIgnoreCase("settings")) {
            state = ((Settings)fragment).getState();
            message = state.getString("message", res.getString(R.string.welcomeMessage));
        }
        savePrefs();
        setMessage();
        setFullScreen();
    }

    /**
     * Handle the cancel button from dialogs.
     * @param fragment the team settings fragment
     */
    public void onDialogNegativeClick(DialogFragment fragment) {
        settings = null;
        setFullScreen();
        victor.run();
    }

    @Override
    public void onPause() {
        Log.d(TAG, "onPause called");
        super.onPause();
        cancelTransitions();
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume called");
        super.onResume();
        sensorsInitialised = false;
        retrievePrefs();
        setMessage();
    }

    /**
     * Save state before device configuration change. Android will handle
     * all views, so we just deal with internal variables.
     * @param state the bundle in which to save variable states
     */
    @Override
    public void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);
        state.putString("message", message);
    }

    public void onSensorChanged(SensorEvent event) {
        int yChange = 0;
        if (!sensorsInitialised) {
            prevAccel[0] = event.values[0];
            prevAccel[1] = event.values[1];
            prevAccel[2] = event.values[2];
            accelChange[0] = 0;
            accelChange[1] = 0;
            accelChange[2] = 0;
            sensorsInitialised = true;
        }
        accelChange[0] = (int)((prevAccel[0] - event.values[0]) * 10);
        accelChange[1] = (int)((prevAccel[1] - event.values[1]) * 10);
        accelChange[2] = (int)((prevAccel[2] - event.values[2]) * 10);
        prevAccel[0] = event.values[0];
        prevAccel[1] = event.values[1];
        prevAccel[2] = event.values[2];
        yChange = (int)accelChange[1];
        Log.d(TAG, prevAccel[0] + "," + accelChange[0] + "," + prevAccel[1] + "," + accelChange[1] + "," + prevAccel[2] + "," + accelChange[2]);
        if (System.currentTimeMillis() - nudgeTime > nudgeDebounce || nudgeTime == 0) {
            if ((int)prevAccel[0] > xGravityLimit) {
                // landscape on 'left' edge
                if (yChange < -nudgeTrigger) {
                    victor.setNextOp(Frankenstein.OP_NEXT_WORD);
                    victor.run();
                    Log.d(TAG, "next word");
                    nudgeTime = System.currentTimeMillis();
                } else
                if (yChange > nudgeTrigger) {
                    victor.setNextOp(Frankenstein.OP_PREV_WORD);
                    victor.run();
                    Log.d(TAG, "prev word");
                    nudgeTime = System.currentTimeMillis();
                }
            } else
            if ((int)prevAccel[0] < -xGravityLimit) {
                if (yChange < -nudgeTrigger) {
                    victor.setNextOp(Frankenstein.OP_PREV_WORD);
                    victor.run();
                    Log.d(TAG, "prev word");
                    nudgeTime = System.currentTimeMillis();
                } else
                if (yChange > nudgeTrigger) {
                    victor.setNextOp(Frankenstein.OP_NEXT_WORD);
                    victor.run();
                    Log.d(TAG, "next word");
                    nudgeTime = System.currentTimeMillis();
                }
            }
        }
    }

    @Override
    public void onStop() {
        savePrefs();
        super.onStop();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return gestureDetector.onTouchEvent(event);
    }

    /**
     * Retrieve application preferences
     */
    private void retrievePrefs() {
        message = appPrefs.getString("message", res.getString(R.string.welcomeMessage));
    }

    /**
     * Save application preferences
     */
    private void savePrefs() {
        SharedPreferences.Editor prefsEditor = appPrefs.edit();
        prefsEditor.putString("message", message);
        prefsEditor.apply();
    }


    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    private void setMessage() {
        messageView.setTextColor(Color.WHITE);
        messageElements = message.split("\\s+");
        elementPointer = 0;
        victor.setMessageText(messageElements[elementPointer]);
        messageView.setAlpha(1);
        messageView.setY(0);
        messageView.setX(0);
        messageView.setRotationX(0);
        messageView.setRotation(0);
        if (displayMode != PitBoard.DISPLAY_SWIPE) {
            sensorMan.unregisterListener(this);
            victor.setNextOp(Frankenstein.OP_NEXT_WORD);
            victor.run();
        } else {
            sensorMan.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

    private void showSettings() {
        Bundle args = new Bundle();
        args.putString("message", message);
        cancelTransitions();
        settings = Settings.newInstance(1);
        settings.setArguments(args);
        settings.show(getFragmentManager(), "settings");
    }



    /**
     * A class to handle animation callbacks
     */
    class Frankenstein implements Runnable {

        private static final String TAG = "PB-FR";
        static final int OP_NEXT_WORD = 0;
        static final int OP_PREV_WORD = 1;
        static final int OP_ANIMATE_OUT = 2;
        static final int OP_DISPLAY_SPACE = 3;
        private int nextOp = OP_NEXT_WORD;

        protected void animateIn() {
            switch (aniInStyle) {
                case ANIMATE_NONE :
                    messageView.setAlpha(1);
                    messageView.setY(0);
                    messageView.setRotationX(0);
                    messageView.setRotation(0);
                    break;
                case ANIMATE_FADE :
                    messageView.setAlpha(0);
                    messageView.setY(0);
                    messageView.setRotationX(0);
                    messageView.setRotation(0);
                    animator.setDuration(aniInDuration).alpha(1);
                    break;
                case ANIMATE_RISE :
                    messageView.setAlpha(1);
                    messageView.setY(displayHeight);
                    messageView.setRotationX(0);
                    messageView.setRotation(0);
                    animator.setDuration(aniInDuration).yBy(-displayHeight);
                    break;
                case ANIMATE_SINK :
                    messageView.setAlpha(1);
                    messageView.setY(-displayHeight);
                    messageView.setRotationX(0);
                    messageView.setRotation(0);
                    animator.setDuration(aniInDuration).yBy(displayHeight);
                    break;
                case ANIMATE_ROLL :
                    messageView.setAlpha(1);
                    messageView.setY(0);
                    messageView.setPivotX(0);
                    messageView.setRotationX(-90);
                    messageView.setRotation(0);
                    animator.setDuration(aniInDuration).rotationXBy(90);
                    break;
                case ANIMATE_HINGE :
                    messageView.setAlpha(1);
                    messageView.setY(0);
                    messageView.setRotationX(0);
                    messageView.setRotation(-90);
                    messageView.setPivotY(0);
                    animator.setDuration(aniInDuration).rotationBy(90);
                    break;
            }
        }

        protected void setNextOp(int op) {
            nextOp = op;
        }

        public void run() {
            if (messageElements.length > 1) {
                // more than one word, so animation required
                switch (nextOp) {
                    case OP_NEXT_WORD :
                        if (displayMode == PitBoard.DISPLAY_SWIPE) {
                            elementPointer++;
                            if (elementPointer >= messageElements.length) {
                                elementPointer = 0;
                            }
                        }
                        animateIn();
                        setMessageText(messageElements[elementPointer]);
                        if (displayMode == PitBoard.DISPLAY_TIMED) {
                            switch (aniOutStyle) {
                                case ANIMATE_NONE :
                                    setNextOp(OP_DISPLAY_SPACE);
                                    timer.postDelayed(victor, wordDuration);
                                    break;
                                default :
                                    setNextOp(OP_ANIMATE_OUT);
                                    timer.postDelayed(victor, (wordDuration - aniOutDuration));
                                    break;
                            }
                            elementPointer++;
                            if (elementPointer >= messageElements.length) {
                                elementPointer = 0;
                            }
                        }
                        break;
                    case OP_PREV_WORD :
                        elementPointer--;
                        if (elementPointer < 0) {
                            elementPointer = messageElements.length - 1;
                        }
                        animateIn();
                        setMessageText(messageElements[elementPointer]);
                        break;
                    case OP_ANIMATE_OUT :
                        setNextOp(OP_DISPLAY_SPACE);
                        switch (aniOutStyle) {
                            case ANIMATE_NONE :
                                // this shouldn't happen
                                messageView.setAlpha(0);
                                run();
                                break;
                            case ANIMATE_FADE :
                                animator.setDuration(aniOutDuration).alpha(0);
                                break;
                            case ANIMATE_RISE :
                                animator.setDuration(aniOutDuration).yBy(-displayHeight);
                                break;
                            case ANIMATE_SINK :
                                animator.setDuration(aniOutDuration).yBy(displayHeight);
                                break;
                            case ANIMATE_ROLL :
                                messageView.setPivotX(0);
                                animator.setDuration(aniOutDuration).rotationXBy(90);
                            case ANIMATE_HINGE :
                                messageView.setPivotY(displayHeight);
                                animator.setDuration(aniOutDuration).rotationBy(90);
                        }
                        timer.postDelayed(victor, aniOutDuration);
                        break;
                    case OP_DISPLAY_SPACE :
                        setNextOp(OP_NEXT_WORD);
                        if (spaceDuration > 0) {
                            timer.postDelayed(victor, spaceDuration);
                        } else {
                            // spaceDuration is zero
                            animator.cancel();
                            run();
                        }
                        break;
                }
            } else {
                messageView.setAlpha(1);
                setMessageText(messageElements[elementPointer]);
            }
        }

        private void setMessageText(String text) {
            float wordWidth = 0;
            messageView.setText(messageElements[elementPointer], TextView.BufferType.NORMAL);
            wordWidth = messageView.getPaint().measureText(messageElements[elementPointer]);
            if (wordWidth > displayWidth) {
                messageView.setTextScaleX(displayWidth / wordWidth);
            } else {
                messageView.setTextScaleX(1);
            }
        }

    }

    class Spider extends GestureDetector.SimpleOnGestureListener implements View.OnTouchListener {

        private static final String TAG = "PB-SP";


        @Override
        public boolean onDoubleTap(MotionEvent event) {
            AboutApp.newInstance(1).show(getFragmentManager(), "about");
            return true;
        }

//        @Override
//        public void onLongPress(MotionEvent event) {
//            showAdvancedSettings();
//        }

        @Override
        public boolean onDown(MotionEvent event) {

            return true;
        }

        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            boolean consumed = false;
            if (displayMode == DISPLAY_SWIPE) {
                if (velocityX < -500) {
                    victor.setNextOp(Frankenstein.OP_NEXT_WORD);
                    victor.run();
                } else
                if (velocityX > 500){
                    victor.setNextOp(Frankenstein.OP_PREV_WORD);
                    victor.run();
                }
                consumed = true;
            }
            return consumed;
        }

        public boolean onSingleTapConfirmed(MotionEvent event) {
            showSettings();
            return true;
        }

        @Override
        public boolean onTouch(View view, MotionEvent event) {
            view.performClick();
            return onTouchEvent(event);
        }

    }

}
