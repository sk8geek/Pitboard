/* 
 * @(#) Settings.java	1.0 	2013/09/28
 * 
 * Copyright 2013 Steven J Lilley
 *
 * A dialog fragment to provide message settings.
 *
 * This file is part of PitBoard.
 *
 * PitBoard is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * steven@channel-e.co.uk
 */
package uk.co.channele.pitboard;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Settings extends DialogFragment implements OnClickListener {
	
	public static final String TAG = "PB-SE";
	private Button[] buttons = new Button[8];
	private EditText messageEditor;
	private String message = "";
	private String[] animations;
	private Resources res;
	private SettingsListener settingsListener;
		
	public interface SettingsListener {
		public void onDialogPositiveClick(DialogFragment dialog);
		public void onDialogNegativeClick(DialogFragment dialog);
    }
		
	public static Settings newInstance(int id) {
		Settings settings = new Settings();
        return settings;
    }
    
    @Override
    public void onAttach(Activity activity) {
    	super.onAttach(activity);
    	try {
    		settingsListener = (SettingsListener)activity;
    	} catch (ClassCastException ccex) {
    		throw new ClassCastException(activity.toString() + " doesn't implement SettingsListener");
    	}
    }
    	
	@Override
	public Dialog onCreateDialog(Bundle state) {
		Builder builder = new Builder(getActivity());
		LayoutInflater inflater = getActivity().getLayoutInflater();
		Dialog picker;
		View view = inflater.inflate(R.layout.settings, null);
		builder.setView(view);
		builder.setPositiveButton(R.string.select, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				settingsListener.onDialogPositiveClick(Settings.this);
			}
		});
		builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				settingsListener.onDialogNegativeClick(Settings.this);
			}
		});      
		res = getResources();
		animations = res.getStringArray(R.array.animations);
		picker = builder.create();
		messageEditor = (EditText)view.findViewById(R.id.messageEditor);
		messageEditor.setText(message, TextView.BufferType.EDITABLE);
		return picker;
	}
	
	public void onClick(View view) {
	}
	
	@Override
	public void onSaveInstanceState(Bundle state) {
		state.putString("message", message);
		super.onSaveInstanceState(state);
	}
	
	@Override
	public void setArguments(Bundle args) {
		message = args.getString("message", message);
	}

	public Bundle getState() {
		Bundle state = new Bundle();
		state.putString("message", messageEditor.getText().toString());
		return state;
	}
	
}

